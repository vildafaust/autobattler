﻿using System.Collections.Generic;
using Descriptions.HeroDescriptions;
using Descriptions.TowerDescriptions;
using Descriptions.UnitDescriptions;
using UnityEngine;

namespace Utilities
{
    public class GameSettings : MonoBehaviour
    {
        [SerializeField, Header("DireTowersSpawnPoints")] private List<TowerPoint> _towersPointDire;
        [SerializeField, Header("RadiantTowersSpawnPoints")] private List<TowerPoint> _towersPointRadiant;
        [SerializeField, Header("SpawnPointsHero")] private HeroSpawnPoints _heroSpawnPoints;
        [SerializeField, Header("LinesWayPoints"),Space] private DescriptionsWayPoints _descriptionsWayPoints;

        [SerializeField, Header("ListTowerDescriptions")] private ListTowerDescriptions listTowerDescriptions;
        [SerializeField, Header("ListUnitDescriptions")]private UnitDescriptions _unitDescriptions;

        public HeroSpawnPoints HeroSpawnPoints => _heroSpawnPoints;
        public UnitDescriptions UnitDescriptions => _unitDescriptions;
        public DescriptionsWayPoints DescriptionsWayPoints => _descriptionsWayPoints;
        public List<TowerPoint> TowersPointDire => _towersPointDire;
        public ListTowerDescriptions ListTowerDescriptions => listTowerDescriptions;
        public List<TowerPoint> TowersPointRadiant => _towersPointRadiant;
    }
}