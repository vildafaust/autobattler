﻿using UnityEngine;
using UnityEngine.UI;

namespace Utilities
{
    public class ViewContainerMenu : MonoBehaviour
    {
        [SerializeField] private Button _startDire;
        [SerializeField] private Button _startRadiant;
        [SerializeField] private Button _random;

        public Button StartDire => _startDire;
        public Button StartRadiant => _startRadiant;
        public Button Random => _random;
    }
}