﻿using System;

namespace Utilities
{
    public class ScoreModel
    {
        public event Action<int> ChangedPoints;

        public int Points { get; private set; } = 30;

        public void SetPoints(int points)
        {
            Points += points;
            ChangedPoints?.Invoke(Points);
        }

        public void RemovePoints(int points)
        {
            Points -= points;
            ChangedPoints?.Invoke(Points);
        }
    }
}