﻿using Player;
using Tower;
using Ui.UiView;
using UnityEngine;
using UnityEngine.UI;

namespace Utilities
{
    public class ViewSessionContainer : MonoBehaviour
    {
        
        [Header("TowerPrefab"),SerializeField] private GameObject _towerPrefab;
        [Header("HeroDire"), SerializeField] private GameObject _heroDirePrefab;
        [Header("HeroRadiant"), SerializeField] private GameObject _heroRadiantPrefab;
        [Space]
        
        [SerializeField, Header("UiPanelUpTower")] private UpPanelView _upPanelView;
        [SerializeField, Header("UiPanelShardPoints")] private PlayerInfoView _playerInfo;
        [SerializeField] private TowerViewUi _towerViewUi;
        [SerializeField] private Button _backToMenu;

        public Button BackToMenu => _backToMenu;
        public TowerViewUi TowerViewUi => _towerViewUi;
        public GameObject HeroRadiantPrefab => _heroRadiantPrefab;
        public GameObject HeroDirePrefab => _heroDirePrefab;
        public PlayerInfoView PlayerInfo => _playerInfo;
        public UpPanelView UpPanelView => _upPanelView;
        public GameObject TowerPrefab => _towerPrefab;
    }
}