﻿using System;
using Managers;
using Units;
using UnityEngine;

namespace DefaultNamespace
{
    public class test
    {
    }

    public interface ICheck
    {
       event Action<UnitModel> fight;
    }

    public class Check : MonoBehaviour, ICheck
    {
        public event Action<UnitModel> fight;
        private readonly UnitManager _unitManager;

        public Check(UnitManager unitManager)
        {
            _unitManager = unitManager;
        }

        private void OnTriggerEnter(Collider other)
        {
            fight?.Invoke(_unitManager.UnitModels[other.gameObject.GetComponent<UnitView>()]);
        }

    }
}