﻿using UnityEngine;
using UnityEngine.UI;

namespace Ui.UiView
{
    public class UpPanelView : MonoBehaviour
    {
        [SerializeField] private Text _price;
        [SerializeField] private Button _levelUp;
        [SerializeField] private Button _close;

        public Button Close => _close;
        public Text Price => _price;
        public Button LevelUp => _levelUp;
    }
}