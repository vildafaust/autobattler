﻿using Units;
using Utilities;

namespace Ui.UiControllers
{
    public class SessionUiController
    {
        private readonly ViewSessionContainer _viewSessionContainer;
        private readonly Context _context;

        public SessionUiController(ViewSessionContainer viewSessionContainer, Context context)
        {
            _viewSessionContainer = viewSessionContainer;
            _context = context;
        }

        public void Init()
        {
            _viewSessionContainer.BackToMenu.onClick.AddListener(LoadMenu);
        }

        private void LoadMenu()
        {
            _context.SceneManager.LoadMenu();
        }

        public void Dispose()
        {
            _viewSessionContainer.BackToMenu.onClick.RemoveListener(LoadMenu);
        }
    }
}