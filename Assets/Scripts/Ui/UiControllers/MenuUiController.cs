﻿using Managers;
using Units;
using UnityEngine;
using Utilities;

namespace Ui.UiControllers
{
    public class MenuUiController
    {
        private readonly ViewContainerMenu _viewContainerMenu;
        private readonly Context _context;
        
        public MenuUiController(ViewContainerMenu viewContainerMenu, Context context)
        {
            _viewContainerMenu = viewContainerMenu;
            _context = context;
        }

        public void Init()
        {
            _viewContainerMenu.StartDire.onClick.AddListener(StartDire);
            _viewContainerMenu.StartRadiant.onClick.AddListener(StartRadiant);
            _viewContainerMenu.Random.onClick.AddListener(StartRandom);
        }

        private void StartRandom()
        {
            var site = Random.Range(0, 1);

            _context.SessionsRules.Site = site == 0 ? "dire" : "radiant";
            LoadSession();
        }

        private void StartRadiant()
        {
            _context.SessionsRules.Site = "radiant";
            LoadSession();
        }

        private void StartDire()
        {
            _context.SessionsRules.Site = "dire";
            LoadSession();
        }

        private void LoadSession()
        {
            _context.SceneManager.LoadSession();
        }
        
        public void Dispose()
        {
            _viewContainerMenu.StartDire.onClick.RemoveListener(StartDire);
            _viewContainerMenu.StartRadiant.onClick.RemoveListener(StartRadiant);
            _viewContainerMenu.Random.onClick.RemoveListener(StartRandom);
        }
    }
}