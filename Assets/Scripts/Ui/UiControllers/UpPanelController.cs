﻿using Managers;
using Tower;
using Ui.UiView;
using UnityEngine;
using Utilities;

namespace Ui.UiControllers
{
    public class UpPanelController
    {
        private readonly UpPanelView _upPanelView;
        private readonly TowerManager _towerManager;
        private readonly ScoreModel _scoreModel;
    
        private bool _flagOpenedPanelUp;
    
        public UpPanelController(UpPanelView upPanelView, TowerManager towerManager, ScoreModel scoreModel)
        {
            _upPanelView = upPanelView;
            _towerManager = towerManager;
            _scoreModel = scoreModel;
        }

        public void Init()
        {
            ClosedPanel();
            _upPanelView.Close.onClick.AddListener(ClosedPanel);
        }

        private TowerModel _activeModel;

        public void OpenPanel(TowerView obj)
        {
            if (_flagOpenedPanelUp)
            {
                _upPanelView.LevelUp.onClick.RemoveListener(LevelUp);
                _activeModel = _towerManager.TowerModels[obj];
            }
            else
            {
                _activeModel = _towerManager.TowerModels[obj];
                _flagOpenedPanelUp = true;
                _upPanelView.gameObject.SetActive(true);
            }        
        
            if (!CheckMaxLevel())
            {
                if (CheckOpportunityLevelUp())
                {
                    _upPanelView.LevelUp.onClick.AddListener(LevelUp);
                }
                RerenderPrice(true);
            }
            else
            {
                RerenderPrice(false);
            }
        }


        private bool CheckMaxLevel()
        {
            return _activeModel.Level >= _activeModel.MaxLevel;
        }

        private void RerenderPrice(bool flag)
        {
            _upPanelView.Price.text = flag ? "" + _activeModel.Description.PriceNext : "MaxLevel";
        }

        private bool CheckOpportunityLevelUp()
        {
            return _activeModel.Description.PriceNext <= _scoreModel.Points;
        }
    
        private void LevelUp()
        {
            if (CheckMaxLevel()) return;
            _scoreModel.RemovePoints(_activeModel.Description.PriceNext);
            _activeModel.LevelUp();

            RerenderPrice(!CheckMaxLevel());
        }

        private void ClosedPanel()
        {
            _flagOpenedPanelUp = false;
            _upPanelView.LevelUp.onClick.RemoveListener(LevelUp);
            _upPanelView.gameObject.SetActive(false);
        }
    }
}