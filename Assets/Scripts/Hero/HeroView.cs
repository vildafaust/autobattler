﻿using UnityEngine;

namespace Hero
{
    public class HeroView : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        
        public void Death()
        {
            _animator.SetBool("dead", true);
        }
    }
}