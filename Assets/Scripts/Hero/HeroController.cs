﻿namespace Hero
{
    public class HeroController
    {
        private readonly HeroModel _heroModel;
        private readonly HeroView _heroView;

        public HeroController(HeroModel heroModel, HeroView heroView)
        {
            _heroModel = heroModel;
            _heroView = heroView;
        }

        public void Init()
        {
            
        }

        private void Death()
        {
            _heroView.Death();
        }
        
        public void Dispose()
        {
            
        }
    }
}