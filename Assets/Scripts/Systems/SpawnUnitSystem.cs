﻿using System.Linq;
using Managers;

namespace Systems
{
    public class SpawnUnitSystem
    {
        private readonly UnitManager _unitManager;
        private readonly TowerManager _towerManager;

        public SpawnUnitSystem(UnitManager unitManager, TowerManager towerManager)
        {
            _unitManager = unitManager;
            _towerManager = towerManager;
        }


        public void Update(float deltaTime)
        {
            foreach (var model in _towerManager.TowerViews.Select(tower => tower.Key).Where(model => !model.Destroyed))
            {
                model.CoolDown -= deltaTime;

                if (model.CoolDown > 0) continue;

                model.CoolDown = model.Description.CoolDown;

                _unitManager.SpawnUnitRadiant(model);
            }
        }
    }
}