﻿using System;
using System.Collections.Generic;
using Tower;
using UnityEngine;

namespace Units
{
    public class UnitView : MonoBehaviour
    {
        public event Action<TowerView, UnitView> Attacked;

        public float Speed;

        public List<Transform> waypoints;
        int curWaypointIndex = 0;
        public float previous_Speed;
        public Animator anim;
        public Transform target;
        public TowerView EnemyTarget;
        private static readonly int Run = Animator.StringToHash("RUN");
        private static readonly int Attack = Animator.StringToHash("Attack");

        [NonSerialized]public string _site ;

        void Start()
        {
            anim = GetComponent<Animator>();
            previous_Speed = Speed;
        }

        void OnTriggerEnter(Collider other)
        {
            if(_site == null) return;
            if (other.CompareTag(_site))
            {
                Speed = 0;
                EnemyTarget = other.gameObject.GetComponent<TowerView>();
                target = other.gameObject.transform;
                Vector3 targetPosition = new Vector3(EnemyTarget.transform.position.x, transform.position.y,
                    EnemyTarget.transform.position.z);
                transform.LookAt(targetPosition);
                anim.SetBool(Run, false);
                anim.SetBool(Attack, true);
            }
        }

        public void GetDamage()
        {
            Attacked?.Invoke(EnemyTarget, this);
        }

        void Update()
        {
            if (curWaypointIndex < waypoints.Count)
            {
                transform.position = Vector3.MoveTowards(transform.position, waypoints[curWaypointIndex].position,
                    Time.deltaTime * Speed);

                if (!EnemyTarget)
                {
                    transform.LookAt(waypoints[curWaypointIndex].position);
                }

                if (Vector3.Distance(transform.position, waypoints[curWaypointIndex].position) < 0.5f)
                {
                    curWaypointIndex++;
                }
            }

            if (EnemyTarget)
            {
                if (EnemyTarget.CompareTag($"Castle_Destroyed"))
                {
                    anim.SetBool("Attack", false);
                    anim.SetBool("RUN", true);
                    Speed = previous_Speed;
                    EnemyTarget = null;
                }
            }
        }
    }
}