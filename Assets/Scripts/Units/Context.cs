﻿using Managers;

namespace Units
{
    public class Context
    {
        private readonly SceneManagerCustom _sceneManager;
        private readonly SessionsRules _sessionsRules;
        
        public Context()
        {
            _sceneManager = new SceneManagerCustom();
            _sessionsRules = new SessionsRules();
        }

        public SessionsRules SessionsRules => _sessionsRules;
        public SceneManagerCustom SceneManager => _sceneManager;
    }
}