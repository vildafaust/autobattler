﻿namespace Units
{
    public class UnitController
    {
        private readonly UnitView _unitView;
        private readonly UnitModel _unitModel;

        public UnitController(UnitView unitView, UnitModel unitModel)
        {
            _unitView = unitView;
            _unitModel = unitModel;
        }

        public void Init()
        {
            
        }

        public void Dispose()
        {
            
        }
    }
}