﻿using Descriptions.UnitDescriptions;

namespace Units
{
    public class UnitModel
    {
        public UnitDescription Description { get; }

        public UnitModel(UnitDescription description)
        {
            Description = description;
        }
    }
}