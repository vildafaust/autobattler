﻿using Systems;
using Managers;
using Player;
using Ui.UiControllers;
using Units;
using UnityEngine;
using Utilities;

namespace SceneControllers
{
    public class SessionSceneController : MonoBehaviour, IScene
    {
        [SerializeField] private GameSettings _gameSettings;
        [SerializeField] private ViewSessionContainer _viewSessionContainer;

        private Context _context;
        
        private TowerManager _towerManager;
        private DialogManager _dialogManager;
        private UnitManager _unitManager;
        private HeroManager _heroManager;

        private SessionUiController _uiController; 
        private ScoreModel _scoreModel;
        private PlayerController _playerController;
        private SpawnUnitSystem _spawnUnitSystem;

        public void Init(Context context)
        {
            _context = context;
            
            _scoreModel = new ScoreModel();
            
            _playerController = new PlayerController(_viewSessionContainer.PlayerInfo, _scoreModel);
            
            _towerManager = new TowerManager(_gameSettings, _viewSessionContainer);
            _dialogManager = new DialogManager(_towerManager, _viewSessionContainer, _scoreModel);
            _unitManager = new UnitManager(_gameSettings, _towerManager);
            _heroManager = new HeroManager(_gameSettings.HeroSpawnPoints, _viewSessionContainer);
            _spawnUnitSystem = new SpawnUnitSystem(_unitManager, _towerManager);

            _uiController = new SessionUiController(_viewSessionContainer, _context);
            
            _uiController.Init();
            _towerManager.Init();
            _playerController.Init();
            _dialogManager.Init();
        }

        public void Dispose()
        {
            _uiController.Dispose();
            _dialogManager.Dispose();
        }

        private void Update()
        {
            _spawnUnitSystem.Update(Time.deltaTime);
        }
    }
}