﻿using Units;

namespace SceneControllers
{
    public interface IScene
    {
        void Init(Context context);
        void Dispose();
    }
}