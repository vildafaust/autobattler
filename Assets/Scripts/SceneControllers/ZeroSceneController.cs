﻿using System;
using Units;
using UnityEngine;

namespace SceneControllers
{
    public class ZeroSceneController : MonoBehaviour
    {
        private Context _context;

        private void Start()
        {
            _context = new Context();
            
            _context.SceneManager.Init(_context);
            _context.SceneManager.LoadMenu();
        }
    }
}