﻿using Ui.UiControllers;
using Units;
using UnityEngine;
using Utilities;

namespace SceneControllers
{
    public class MenuSceneController : MonoBehaviour, IScene
    {
        [SerializeField] private ViewContainerMenu _containerMenu;
        
        private MenuUiController _menuUiController;
        private Context _context;
        
        public void Init(Context context)
        {
            _context = context;

            _menuUiController = new MenuUiController(_containerMenu, context);
            
            _menuUiController.Init();
        }

        public void Dispose()
        {
            _menuUiController.Dispose();
        }
    }
}