﻿using UnityEngine;

namespace Descriptions.HeroDescriptions
{
    public class HeroSpawnPoints : MonoBehaviour
    {
        [SerializeField] private Transform _direHeroPoint;
        [SerializeField] private Transform _radiantHeroPoint;

        [SerializeField] private Transform _direHeroHealthBar;
        [SerializeField] private Transform _radiantHeroHeatlhBar;

        public Transform DireHeroHealthBar => _direHeroHealthBar;
        public Transform RadiantHeroHeatlhBar => _radiantHeroHeatlhBar;
        public Transform DireHeroPoint => _direHeroPoint;
        public Transform RadiantHeroPoint => _radiantHeroPoint;
    }
}