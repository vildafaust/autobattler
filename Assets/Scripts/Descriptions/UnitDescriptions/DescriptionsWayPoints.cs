﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Descriptions.UnitDescriptions
{
    public class DescriptionsWayPoints : MonoBehaviour
    {
        [SerializeField, Header("WayPointsDire")] private List<LineWayPoints> _lineWayPointsesDire;
        [SerializeField, Header("WayPointsRadiant")] private List<LineWayPoints> _lineWayPointsesRadiant;
        
        [SerializeField, Header("FinalRadiantPoint"),Space] private Transform _lastWayPointRadianе;
        [SerializeField, Header("FinalDirePoint")] private Transform _lastWayPointDire;

        private void Awake()
        {
            foreach (var list in _lineWayPointsesDire)
            {
                list.WayPoints.Add(_lastWayPointDire);
            }

            foreach (var list in _lineWayPointsesRadiant)
            {
                list.WayPoints.Add(_lastWayPointRadianе);
            }
        }

        public List<LineWayPoints> LineWayPointsesDire => _lineWayPointsesDire;
        public List<LineWayPoints> LineWayPointsesRadiant => _lineWayPointsesRadiant;
        
    }
}