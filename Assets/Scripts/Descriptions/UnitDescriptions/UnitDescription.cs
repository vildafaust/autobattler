﻿using System;
using UnityEngine;

namespace Descriptions.UnitDescriptions
{
    [Serializable]
    public class UnitDescription
    {
        [SerializeField] private GameObject _unitPrefab;
        [SerializeField] private int _unitId;
        [SerializeField] private float _damage;
        [SerializeField] private float _maxHealth;

        public GameObject UnitPrefab => _unitPrefab;
        public int UnitId => _unitId;
        public float Damage => _damage;
        public float MaxHealth => _maxHealth;
    }
}