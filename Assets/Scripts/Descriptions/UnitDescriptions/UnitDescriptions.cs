﻿using System.Collections.Generic;
using UnityEngine;

namespace Descriptions.UnitDescriptions
{
    [CreateAssetMenu]
    public class UnitDescriptions : ScriptableObject
    {
        [SerializeField] private List<UnitDescription> _descriptions;

        public List<UnitDescription> Descriptions => _descriptions;
    }
}