﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Descriptions.UnitDescriptions
{
    [Serializable]
    public class LineWayPoints
    {
        public List<Transform> WayPoints;
    }
}