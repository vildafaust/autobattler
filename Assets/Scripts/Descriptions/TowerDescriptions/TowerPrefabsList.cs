﻿using System.Collections.Generic;
using UnityEngine;

namespace Descriptions.TowerDescriptions
{
    [CreateAssetMenu]
    public class TowerList : ScriptableObject
    {
        [SerializeField] private List<GameObject> _towers;

        public List<GameObject> Towers => _towers;
    }
}