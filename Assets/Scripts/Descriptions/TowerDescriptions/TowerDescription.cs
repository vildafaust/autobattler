﻿using System;

namespace Descriptions.TowerDescriptions
{
    [Serializable]
    public class TowerDescription
    {
        public float CoolDown;
        public int IdUnit;
        public float RadiusSpawn;
        public int PriceNext;
        public float MaxHealth;
    }
}