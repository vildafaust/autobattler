﻿using System.Collections.Generic;
using UnityEngine;

namespace Descriptions.TowerDescriptions
{
    [CreateAssetMenu]
    public class ListTowerDescriptions : ScriptableObject
    {
        [SerializeField] private List<TowerDescription> _towerDescriptions;

        public List<TowerDescription> TowerDescriptions => _towerDescriptions;
    }
}