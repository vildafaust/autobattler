﻿using System;
using UnityEngine;

namespace Descriptions.TowerDescriptions
{
    [Serializable]
    public class TowerPoint
    {
        public Transform PositionSpawn;
        public Transform PositionSpawnHealthBar;
        public string NameLine;
    }
}