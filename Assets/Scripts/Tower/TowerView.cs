﻿using System;
using System.Collections.Generic;
using Descriptions.TowerDescriptions;
using UnityEngine;

namespace Tower
{
    public class TowerView : MonoBehaviour
    {
        public event Action<TowerView> Clicked;

        [SerializeField] private List<GameObject> _towerList;

        private GameObject _currentTower;

        private bool _enemy;

        private void Start()
        {
            _currentTower = Instantiate(_towerList[0], transform);
        }

        public void Enable()
        {
            _enemy = true;
        }
        
        private void OnMouseDown()
        {
            if(_enemy) return;
            Clicked?.Invoke(this);
        }

        public void LevelUp(int level)
        {
            Destroy(_currentTower);
            _currentTower = Instantiate(_towerList[level], transform);
        }

        public void Death()
        {
            gameObject.tag = "Castle_Destroyed";
            gameObject.SetActive(false);
        }
    }
}