﻿using UnityEngine;
using UnityEngine.UI;

namespace Tower
{
    public class TowerViewUi : MonoBehaviour
    {
        [SerializeField] private Slider _slider;

        public void ChangeMaxHealth(float maxHealth)
        {
            _slider.maxValue = maxHealth;
            _slider.value = maxHealth;
        }

        public void ChangeHeath(float health)
        {
            _slider.value = health;
        }

        public void DeActive()
        {
            gameObject.SetActive(false);
        }
        

    }
}