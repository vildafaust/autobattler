﻿using System;
using System.Collections.Generic;
using Descriptions.TowerDescriptions;
using UnityEngine;

namespace Tower
{
    public class TowerModel
    {
        public event Action<int> LevelUpped;
        public event Action Destroy;
        public event Action<float> ChangedLevel; 
        public event Action<float> ChangedHealth;
        
        public string Site { get; }
        public bool Destroyed { get; private set; }
        public TowerDescription Description { get; private set; }
        public float CoolDown;
        public float CurrentHealth { get; private set; }
        public int Level { get; private set; }
        public int MaxLevel { get; } = 2;
        private List<TowerDescription> _towerDescriptions;
    
        public TowerModel(List<TowerDescription> towerDescriptions, string site)
        {
            Site = site;
            _towerDescriptions = towerDescriptions;
            Level = 0;
            Description = _towerDescriptions[Level];
            CurrentHealth = Description.MaxHealth;
        }


        public void LevelUp()
        {
            Level++;
            Description = _towerDescriptions[Level];
            CurrentHealth = Description.MaxHealth;
            ChangedLevel?.Invoke(CurrentHealth);
            LevelUpped?.Invoke(Level);
        }

        public void SetDamage(float damage)
        {
            CurrentHealth -= damage;
            ChangedHealth?.Invoke(CurrentHealth);
            if (CurrentHealth <= 0)
            {
                Destroyed = true;
                Destroy?.Invoke();
            }
        }
    }
}