﻿namespace Tower
{
    public class TowerController
    {
        private readonly TowerModel _towerModel;
        private readonly TowerView _towerView;
        private readonly TowerViewUi _towerViewUi;
        
        public TowerController(TowerModel towerModel, TowerView towerView, TowerViewUi towerViewUi)
        {
            _towerModel = towerModel;
            _towerView = towerView;
            _towerViewUi = towerViewUi;
        }

        public void Init()
        {
            ChangedLevel(_towerModel.CurrentHealth);
            
            _towerModel.LevelUpped += LevelUp;
            _towerModel.Destroy += Destroy;
            _towerModel.ChangedHealth += ChangedHealth;
            _towerModel.ChangedLevel += ChangedLevel;
        }

        private void ChangedLevel(float obj)
        {
            _towerViewUi.ChangeMaxHealth(obj);
        }

        private void ChangedHealth(float obj)
        {
            _towerViewUi.ChangeHeath(obj);
        }

        private void Destroy()
        {
            _towerViewUi.DeActive();
            _towerView.Death();
        }

        private void LevelUp(int level)
        {
            _towerView.LevelUp(level);
        }

        public void Dispose()
        {
            _towerModel.LevelUpped -= LevelUp;
            _towerModel.Destroy -= Destroy;
        }
    }
}