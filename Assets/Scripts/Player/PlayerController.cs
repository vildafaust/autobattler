﻿using Utilities;

namespace Player
{
    public class PlayerController
    {
        private readonly PlayerInfoView _playerInfo;
        private readonly ScoreModel _model;

        public PlayerController(PlayerInfoView playerInfo, ScoreModel scoreModel)
        {
            _playerInfo = playerInfo;
            _model = scoreModel;
        }

        public void Init()
        {
            _playerInfo.SetPoints(_model.Points);
            _model.ChangedPoints += SetPoints;
        }

        private void SetPoints(int points)
        {
            _playerInfo.SetPoints(points);
        }
    }
}