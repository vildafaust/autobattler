﻿using UnityEngine;
using UnityEngine.UI;

namespace Player
{
    public class PlayerInfoView : MonoBehaviour
    {
        [SerializeField] private Text _points;

        public void SetPoints(int points)
        {
            _points.text = "" + points;
        }
    }
}