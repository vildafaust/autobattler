﻿using SceneControllers;
using Units;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Managers
{
    public class SceneManagerCustom
    {
        private Context _context;
        private IScene _currentScene;

        public void Init(Context context)
        {
            _context = context;
        }

        public void LoadMenu()
        {
            if (_currentScene != null)
            {
                _currentScene.Dispose();
                SceneManager.UnloadSceneAsync("Session");
            }

            var loader = SceneManager.LoadSceneAsync("Menu");
            loader.completed += InitScene;
        }


        public void LoadSession()
        {
            _currentScene.Dispose();
            SceneManager.UnloadSceneAsync("Menu");
            var loader = SceneManager.LoadSceneAsync("Session");
            loader.completed += InitScene;
        }

        private void InitScene(AsyncOperation obj)
        {
            _currentScene = GameObject.FindWithTag("SceneController").GetComponent<IScene>();
            _currentScene.Init(_context);
        }
    }
}