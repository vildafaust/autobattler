﻿using System.Collections.Generic;
using Descriptions.HeroDescriptions;
using Hero;
using Tower;
using UnityEngine;
using Utilities;

namespace Managers
{
    public class HeroManager
    {
        private readonly HeroSpawnPoints _heroSpawnPoints;
        private readonly ViewSessionContainer _viewSessionContainer;
        
        private Dictionary<HeroView, HeroModel> _heroModels;
        private Dictionary<HeroModel, HeroController> _heroControllers;

        public HeroManager(HeroSpawnPoints spawnPoints, ViewSessionContainer viewSessionContainer)
        {
            _heroSpawnPoints = spawnPoints;
            _viewSessionContainer = viewSessionContainer;
            SpawnHeroDire();
            SpawnHeroRadiant();
        }

        private void SpawnHeroDire()
        {
            var model = new HeroModel();
            var view = Object.Instantiate(_viewSessionContainer.HeroDirePrefab, _heroSpawnPoints.DireHeroPoint).GetComponent<HeroView>();
            var controller = new HeroController(model, view);
            var viewUi = Object.Instantiate(_viewSessionContainer.TowerViewUi, _heroSpawnPoints.DireHeroHealthBar)
                .GetComponent<TowerViewUi>();
            controller.Init();
        }
        
        private void SpawnHeroRadiant()
        {
            var model = new HeroModel();
            var view = Object.Instantiate(_viewSessionContainer.HeroRadiantPrefab, _heroSpawnPoints.RadiantHeroPoint).GetComponent<HeroView>();
            var viewUi = Object.Instantiate(_viewSessionContainer.TowerViewUi, _heroSpawnPoints.RadiantHeroHeatlhBar).GetComponent<TowerViewUi>();
            var controller = new HeroController(model, view);
            
            controller.Init();
        }
    }
}