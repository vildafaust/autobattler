﻿using Ui.UiControllers;
using Utilities;

namespace Managers
{
    public class DialogManager
    {
        private readonly TowerManager _towerManager;
        private readonly ViewSessionContainer _viewSessionContainer;

        private readonly UpPanelController _upPanelController;

        public DialogManager(TowerManager towerManager, ViewSessionContainer viewSessionContainer, ScoreModel scoreModel)
        {
            _towerManager = towerManager;
            _viewSessionContainer = viewSessionContainer;

            _upPanelController = new UpPanelController(_viewSessionContainer.UpPanelView, _towerManager, scoreModel);
        }

        public void Init()
        {
            _upPanelController.Init();
            Binding();
        }

        private void Binding()
        {
            foreach (var towerView in _towerManager.TowerViews.Values)
            {
                towerView.Clicked += _upPanelController.OpenPanel;
            }
        }

        public void Dispose()
        {
            foreach (var towerView in _towerManager.TowerViews.Values)
            {
                towerView.Clicked -= _upPanelController.OpenPanel;
            }
        }
    }
}