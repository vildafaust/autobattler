﻿using System;
using System.Collections.Generic;
using Descriptions.UnitDescriptions;
using Tower;
using Units;
using UnityEngine;
using Utilities;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

namespace Managers
{
    public class UnitManager
    {
        private readonly GameSettings _gameSettings;
        private readonly TowerManager _towerManager;

        private readonly Dictionary<string, Func<List<Transform>>> _linesWayPointsRadiant;
        private readonly Dictionary<string, Func<List<Transform>>> _linesWayPointsDire;
        

        public Dictionary<UnitView, UnitModel> UnitModels;

        public UnitManager(GameSettings gameSettings, TowerManager towerManager)
        {
            _gameSettings = gameSettings;
            _towerManager = towerManager;

            UnitModels = new Dictionary<UnitView, UnitModel>();
            _linesWayPointsRadiant = new Dictionary<string, Func<List<Transform>>>()
            {
                {"Top", () => _gameSettings.DescriptionsWayPoints.LineWayPointsesRadiant[0].WayPoints},
                {"Middle", () => _gameSettings.DescriptionsWayPoints.LineWayPointsesRadiant[1].WayPoints},
                {"Bottom", () => _gameSettings.DescriptionsWayPoints.LineWayPointsesRadiant[2].WayPoints},
            };

            _linesWayPointsDire = new Dictionary<string, Func<List<Transform>>>()
            {
                {"Top", () => _gameSettings.DescriptionsWayPoints.LineWayPointsesDire[0].WayPoints},
                {"Middle", () => _gameSettings.DescriptionsWayPoints.LineWayPointsesDire[1].WayPoints},
                {"Bottom", () => _gameSettings.DescriptionsWayPoints.LineWayPointsesDire[2].WayPoints},
            };
        }


        public void SpawnUnitRadiant(TowerModel towerValue)
        {
            var view = _towerManager.TowerViews[towerValue];
            var position = view.transform.position;
            var dopX = Random.Range(1, 3);
            var dopZ = Random.Range(1, 3);
            
            var idUnit = towerValue.Description.IdUnit;
            var unitDescriptions = _gameSettings.UnitDescriptions.Descriptions[idUnit];
        
            var unitView = Object.Instantiate(unitDescriptions.UnitPrefab, new Vector3(position.x + dopX,
                position.y, position.z + dopZ), Quaternion.identity).GetComponent<UnitView>();
            
            if (towerValue.Site == "dire")
            {
                unitView._site = "Castle_radiant";
                
                unitView.waypoints = _linesWayPointsRadiant[_towerManager.TowerLines[towerValue]]();

                unitView.Attacked += DamageTower;
            }
            else
            {
                unitView._site = "Castle_dire";

                unitView.waypoints = _linesWayPointsDire[_towerManager.TowerLines[towerValue]]();
                
                unitView.Attacked += DamageTower;
            }
            
            var model = new UnitModel(unitDescriptions);

            UnitModels[unitView] = model;
        }

        private void DamageTower(TowerView towerView, UnitView unitView)
        {
            var damage = UnitModels[unitView].Description.Damage;
            _towerManager.TowerModels[towerView].SetDamage(damage);
        }
    }
}