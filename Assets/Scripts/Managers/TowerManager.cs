﻿using System.Collections.Generic;
using Tower;
using UnityEngine;
using Utilities;

namespace Managers
{
    public class TowerManager
    {
        private readonly GameSettings _gameSettings;
        private readonly ViewSessionContainer _viewSessionContainer;
        
        private readonly Dictionary<TowerModel, TowerController> _towerControllers;
        public readonly Dictionary<TowerView, TowerModel> TowerModels;
        public readonly Dictionary<TowerModel, TowerView> TowerViews;
        public readonly Dictionary<TowerModel, string> TowerLines;


        public TowerManager(GameSettings gameSettings, ViewSessionContainer viewSessionContainer)
        {
            _gameSettings = gameSettings;
            _viewSessionContainer = viewSessionContainer;
            
            TowerViews = new Dictionary<TowerModel, TowerView>();
            _towerControllers = new Dictionary<TowerModel, TowerController>();
            TowerLines = new Dictionary<TowerModel, string>();
            TowerModels = new Dictionary<TowerView, TowerModel>();
        }

        public void Init()
        {
            CreateTowers();
            InitTowers();
        }

        private void CreateTowers()
        {
            foreach (var position in _gameSettings.TowersPointRadiant)
            {
                var model = new TowerModel(_gameSettings.ListTowerDescriptions.TowerDescriptions, "radiant");
                var view = Object.Instantiate(_viewSessionContainer.TowerPrefab, position.PositionSpawn)
                    .GetComponent<TowerView>();
                
                view.gameObject.tag = "Castle_radiant";
                view.Enable();
                
                var viewUi = Object.Instantiate(_viewSessionContainer.TowerViewUi, position.PositionSpawnHealthBar)
                    .GetComponent<TowerViewUi>();
                
                var controller = new TowerController(model, view,viewUi);

                _towerControllers[model] = controller;
                TowerViews[model] = view;
                TowerLines[model] = position.NameLine;
                TowerModels[view] = model;
            }

            foreach (var position in _gameSettings.TowersPointDire)
            {
                var model = new TowerModel(_gameSettings.ListTowerDescriptions.TowerDescriptions, "dire");

                var view = Object.Instantiate(_viewSessionContainer.TowerPrefab, position.PositionSpawn)
                    .GetComponent<TowerView>();
                
                var viewUi = Object.Instantiate(_viewSessionContainer.TowerViewUi, position.PositionSpawnHealthBar)
                    .GetComponent<TowerViewUi>();
                view.gameObject.tag = "Castle_dire";

                var controller = new TowerController(model, view,viewUi);

                _towerControllers[model] = controller;
                TowerLines[model] = position.NameLine;
                TowerViews[model] = view;
                TowerModels[view] = model;
            }
        }

        private void InitTowers()
        {
            foreach (var controller in _towerControllers.Values)
            {
                controller.Init();
            }
        }

        public void Dispose()
        {
            foreach (var controller in _towerControllers.Values)
            {
                controller.Dispose();
            } 
        } 
    }
}